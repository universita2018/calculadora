
var operandoa;
var operandob;
var operacion;
function init(){
  //variables
  var resultado = document.getElementById('resultado');
  var reset = document.getElementById('reset');
  var suma = document.getElementById('suma');
  var resta = document.getElementById('resta');
  var multiplicacion = document.getElementById('multiplicacion');
  var division = document.getElementById('division');
  var igual = document.getElementById('igual');
  var uno = document.getElementById('uno');
  var dos = document.getElementById('dos');
  var tres = document.getElementById('tres');
  var cuatro = document.getElementById('cuatro');
  var cinco = document.getElementById('cinco');
  var seis = document.getElementById('seis');
  var siete = document.getElementById('siete');
  var ocho = document.getElementById('ocho');
  var nueve = document.getElementById('nueve');
  var cero = document.getElementById('cero');

  var raiz = document.getElementById('raiz');
  var potencia = document.getElementById('potencia');
  var seno = document.getElementById('seno');
  var coseno = document.getElementById('coseno');
  var tangente=document.getElementById('tangente');
  var decimal=document.getElementById('decimal');
  var parentesis=document.getElementById('parentesis');
}
//Eventos de click
  uno.onclick = function(e){
      resultado.textContent = resultado.textContent  + "1";
  }
  dos.onclick = function(e){
      resultado.textContent = resultado.textContent  + "2";
  }
  tres.onclick = function(e){
      resultado.textContent = resultado.textContent  + "3";
  }
  cuatro.onclick = function(e){
      resultado.textContent = resultado.textContent  + "4";
  }
  cinco.onclick = function(e){
      resultado.textContent = resultado.textContent  + "5";
  }
  seis.onclick = function(e){
      resultado.textContent = resultado.textContent  + "6";
  }
  siete.onclick = function(e){
      resultado.textContent = resultado.textContent  + "7";
  }
  ocho.onclick = function(e){
      resultado.textContent = resultado.textContent  + "8";
  }
  nueve.onclick = function(e){
      resultado.textContent = resultado.textContent  + "9";
  }
  cero.onclick = function(e){
      resultado.textContent = resultado.textContent  + "0";
  }
  decimal.onclick=function(e){
        resultado.textContent = resultado.textContent  + ".";
        
        
    }
    parentesis.onclick=function(e){
        
        resultado.textContent = "("+resultado.textContent  + ")";
        resultado.textContent.value=resultado;
        if(parentesis=="("&& parentesis==""){
            operandoa=resultado.textContent.value+" "+operacion;
        }else
        {
            operandob=resultado.textContent.value;
            resolver();
        }
        
        
    }
    
  reset.onclick = function(e){
      resetear();
  }
  suma.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "+";
      limpiar();
  }
  resta.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "-";
      limpiar();
  }
  multiplicacion.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "*";
      limpiar();
  }
  division.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "/";
      limpiar();
  }
   raiz.onclick = function(e){
      operandoa = resultado.textContent;
      operacion = "r";
      limpiar();
    }
  potencia.onclick = function(e){
    operandoa = resultado.textContent;
    operacion = "p";
    limpiar();
    } 
    seno.onclick = function(e){
        operandoa = resultado.textContent;
        operacion = "s";
        limpiar();
        }  
    coseno.onclick = function(e){
        operandoa = resultado.textContent;
        operacion = "c";
        limpiar();
        } 
    tangente.onclick = function(e){
        operandoa = resultado.textContent;
        operacion = "c";
        limpiar();
        }     
    
    
  igual.onclick = function(e){
      operandoa = resultado.textContent;
      resolver();
  }
function limpiar(){
  resultado.textContent = "";
}
function resetear(){
  resultado.textContent = "";
  operandoa = 0;
  operandob = 0;
  operacion = "";
}
function resolver(){
  var res = 0;
  switch(operacion){
    case "+":
       res=parseFloat(operandoa) + parseFloat(operandob);
    
      break;
    case "-":
        res = parseFloat(operandoa) - parseFloat(operandob);
        break;
    case "*":
      res = parseFloat(operandoa) * parseFloat(operandob);
      break;
    case "/":
    if(operandob=="0")
    {
       alert("ERROR"); 
    }else{
       res = parseFloat(operandoa) / parseFloat(operandob) 
    }
      ;
      break;
    case "r":
    if(operandoa<=0){
        alert("ERROR no hay RAIZ de Numeros negativos");
    }else{

  
    res = Math.sqrt(parseFloat(operandoa));
    }
      break;
    case "p":
    res = Math.pow((parseFloat(operandoa)),(parseFloat(operandob)));
      break;
    case "s":
    res = Math.sin(parseFloat(operandoa));
      break;
    case "c":
    res = Math.cos(parseFloat(operandoa))*(180/Math.PI);
      break;
    case "t":
    res=(Math.tan(parseFloat(operandoa)));
    break;
    
  }
  resetear();
  resultado.textContent = res;
}
